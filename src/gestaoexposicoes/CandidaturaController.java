/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestaoexposicoes;

/**
 *
 * @author Andre Pato
 */
public class CandidaturaController {

    
  
    private CentroExposicoes ca_centroExposicoes;
    /**
     * cria uma nova cndidatura
     */
    private Candidatura ca_candidatura;
    /**
     * construtor completo
     * @param ca_centroExposicoes 
     */
    public CandidaturaController(CentroExposicoes ca_centroExposicoes){
        this.ca_centroExposicoes=ca_centroExposicoes;
    }
    /**
     * cria uma nova candidatura
     */
    public void NewCandidatura(){
        
    }
    /**
     * define os dados da candidatura
     */
    public void setDadosCandidatura(){
        
    }    
    /**
     * confirma candidatura
     */
    public void confirmaCandidatura(){
    
    }    
    /**
     * valida os dados da candidatura
     */
    public void validaCandidatura(){
        
    }
    /**
     * mostra uma candidatura
     */
    public void getCandidatura(){
        
    }
    /**
     * Define a decisao sobre a candidatura
    
    */
    public void setDecisaoCandidatura(){
        
    }
    /**
     * valida a decisao da candidatura
     */
    public void validaDecisaoCandidatura(){
        
    } 
}
