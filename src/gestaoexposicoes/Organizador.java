package gestaoexposicoes;

/**
 *
 * @author teofilomatos
 */
public class Organizador {
    
    private Utilizador m_Utilizador;
    
    public Organizador()
    {
    }
    
    public void setUtilizador(Utilizador u)
    {
        this.m_Utilizador = u;
    }
 
    public Utilizador getUtilizador()
    {
        return m_Utilizador;
    }
 
    @Override
    public String toString()
    {
        String str = "Organizador:\n";
        str += "\tUser: " + this.m_Utilizador.getUser() + "\n";

        return str;
    }
}
