
package gestaoexposicoes;

/**
 *
 * @author Andre Pato
 */
public class RegistarUtilizadorUI {
    
    private CentroExposicoes ru_centroExposicao;
  
    
    private RegistarUtilizadorController ru_utilizadorController;
    /**
     * construtor completo
     * @param ru_centroExposicoes 
     */
    public RegistarUtilizadorUI(CentroExposicoes ru_centroExposicoes){
        this.ru_centroExposicao=ru_centroExposicoes;
        ru_utilizadorController=new RegistarUtilizadorController(ru_centroExposicoes);
    }
    /**
     * interface com o utilizador
     */
    public void run(){
        
    }
    /**
     * cria novo utilizador
     * 
     */
    private void novoUtilizador(){
        
    }
    
    /**
     * Define os dados do utilizador
     */
    private void setDados(){
        
    }
    /**
     * mostra os dados do utilizador
     */
    private void getDados(){
        
    }
    /**
     * Valida os dados do utilizador
     */
    private void validaUtilizador(){
        
    }
    /**
     * mostra a lista de registos efetuados
     */
    private void getListaRegistos(){
        
    }
    /**
     * define a decisao feita pelo utilizador
     */
    private void setDecisaoUtilizador(){
        
    }
    
}
