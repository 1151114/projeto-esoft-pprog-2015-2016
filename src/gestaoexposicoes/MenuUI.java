package gestaoexposicoes;

import java.io.IOException;
import utils.Utils;

/**
 *
 * @author teofilomatos
 */
public class MenuUI {
    
    private CentroExposicoes m_centro_exposicoes;
    private String opcao;

    public MenuUI(CentroExposicoes centro_exposicoes)
    {
        m_centro_exposicoes = centro_exposicoes;
    }

    public void run() throws IOException
    {
        do
        {
            System.out.println("\n\n");
            System.out.println("1. Criar Exposição");
            System.out.println("2. Definir FAE");
            System.out.println("3. Atribuir Candidatura");
            System.out.println("4. Decidir Candidaturas");
            System.out.println("5. Registar Candidatura");
            System.out.println("6. Registar Utilizador");
            System.out.println("7. Confirmar Registo de Utilizador");
            
            System.out.println("0. Sair");

            opcao = Utils.readLineFromConsole("Introduza opção: ");

            if( opcao.equals("1") )
            {
                CriarExposicaoUI uiCE = new CriarExposicaoUI(m_centro_exposicoes);
                uiCE.run();
            }
            
            if(opcao.equals("2")){
                 DefinirFAEUI uiFAE = new DefinirFAEUI(m_ce);
                 uiFAE.run();
            }

        }
        while (!opcao.equals("0") );
    }
}
