package gestaoexposicoes;

/**
 *
 * @author teofilomatos
 */
public class Utilizador {
    
    private String m_strNome;
    private String m_strUser;
    private String m_strPwd;
    private String m_strEmail;

    public Utilizador()
    {
    }
    
    public void setNome(String strNome)
    {
        this.m_strNome = strNome;
    }

    public void setUser(String strUser)
    {
        m_strUser = strUser;
    }
    
    public String getUser()
    {
        return m_strUser;
    }

    public void setPwd(String strPwd)
    {
        m_strPwd = strPwd;
    }

    public void setEmail(String strEmail)
    {
        this.m_strEmail = strEmail;
    }

    public boolean valida()
    {
        System.out.println("Utilizador: valida: " + this.toString());
        return true;
    }
    
    public String mostraSimples(){
        return m_strUser+"-"+m_strNome;
    }
            
    @Override
    public String toString()
    {
        String str = "Utilizador:\n";
        str += "\tNome: " + this.m_strNome + "\n";
        str += "\tUser: " + this.m_strUser + "\n";
        str += "\tPwd: " + this.m_strPwd + "\n";
        str += "\tEmail: " + this.m_strEmail + "\n";

        return str;
    }
}
