package gestaoexposicoes;

import java.util.*;

/**
 *
 * @author teofilomatos
 */
public final class CentroExposicoes {
 
    private final List<Utilizador> m_listaUtilizadores;
    private final List<Exposicao> m_listaExposicoes;
    
    
    public CentroExposicoes(){
        m_listaUtilizadores = new ArrayList<Utilizador>();
        m_listaExposicoes = new ArrayList<Exposicao>();
        
        fillInData(); // preencher com dados para teste.
    }
    
    void fillInData(){
        
        Utilizador u = new Utilizador();
        u.setNome("João");u.setEmail("joao@esoft.pt");u.setUser("joao");u.setPwd("abc");
        m_listaUtilizadores.add(u);
        
        Utilizador u2 = new Utilizador();
        u2.setNome("ricardo");u2.setEmail("ricardo@esoft.pt");u2.setUser("ricardo");u2.setPwd("def");
        m_listaUtilizadores.add(u2);
        
        Utilizador u3 = new Utilizador();
        u3.setNome("pedro");u3.setEmail("pedro@esoft.pt");u3.setUser("pedro");u3.setPwd("ghi");
        m_listaUtilizadores.add(u3);
        
        Utilizador u4 = new Utilizador();
        u4.setNome("joaquim");u4.setEmail("joaquim@esoft.pt");u4.setUser("joaquim");u4.setPwd("jkl");
        m_listaUtilizadores.add(u4);
        
        Utilizador u5 = new Utilizador();
        u5.setNome("Bruno");u5.setEmail("bruno@esoft.pt");u5.setUser("bruno");u5.setPwd("mno");
        m_listaUtilizadores.add(u5);
    }
            
    public Exposicao novaExposicao()
    {
        return new Exposicao();
    }
    
    public boolean registaExposicao(Exposicao e)
    {
        if( validaExposicao(e) )
            return addExposicao(e);
        else
            return false;
    }
    
    private boolean addExposicao(Exposicao e)
    {
        return m_listaExposicoes.add(e);
    }
    
    public boolean validaExposicao(Exposicao e)
    {
        return e.valida();
    }
    
    public List<Utilizador> getUtilizadores(){
        return m_listaUtilizadores;
    }
}
