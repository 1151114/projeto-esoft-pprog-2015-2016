package gestaoexposicoes;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import utils.Utils;

/**
 *
 * @author teofilomatos
 */
public class CriarExposicaoUI {
 
    private final CentroExposicoes m_centro_exposicoes;
    private final CriarExposicaoController m_controllerCE;

    public CriarExposicaoUI( CentroExposicoes centro_exposicoes )
    {
        m_centro_exposicoes = centro_exposicoes;
        m_controllerCE = new CriarExposicaoController(m_centro_exposicoes);
    }
    
    public void run()
    {
        novaExposicao();
        
        List<Utilizador> lu = m_controllerCE.getListaUtilizadores();
        
        Exposicao exposicao = introduzDadosExposicao(lu);
        
        // apresenta dados
        System.out.println(exposicao.toString());
        
        // solicita confirmação
        if( Utils.confirmacao("Confirma dados (s/n)?").equals("s")){
            if(m_controllerCE.registaExposicao()){
                System.out.println("A exposição foi registada: "+exposicao.toString());
            }
            else{
                System.out.println("A exposição não foi registada por problema interno!");
            }
        }else{
            System.out.println("A exposição não foi registada por decisão do utilizador!");
        }
    }
    
    private void novaExposicao()
    {
        m_controllerCE.novaExposicao();
    }
    
    private Exposicao introduzDadosExposicao(List<Utilizador> lu)
    {
        System.out.println("Introdução de dados da exposição");
        
        String strTitulo = Utils.readLineFromConsole("Título: ");
        String strDescritivo = Utils.readLineFromConsole("Descritivo: ");
        Date strDataIni = Utils.readDateFromConsole("Data inicial (dd-mm-aaaa): ");
        Date strDataFim = Utils.readDateFromConsole("Data final (dd-mm-aaaa): ");
        String strLocal = Utils.readLineFromConsole("Local: ");
        
        // escolher os utilizadores
        List<Utilizador> luEscolhidos = new ArrayList<Utilizador>();
        String user;
        
        do
        {
            // apresentar os utilizadores
            System.out.println("-- Utilizadores --");
            for(Utilizador u:lu){
                if( !existeLista(u,luEscolhidos) ){
                    System.out.println(u.mostraSimples());
                }
            }
       
            user = Utils.readLineFromConsole("Escolha pelo user (0-terminar):");

            if( !user.equals("0") )
            {
                Utilizador u = existe(user,lu);
                if (u!=null){
                    luEscolhidos.add(u);    
                }
            }

        }
        while (!user.equals("0") );
        
        return m_controllerCE.setDados(strTitulo, strDescritivo, strDataIni, strDataFim, strLocal, luEscolhidos);
    }
    
    private boolean existeLista(Utilizador ut, List<Utilizador> lu)
    {
	for(Utilizador u : lu) {
            if( u != null && u.getUser().equals(ut.getUser())) {
                return true;
            }
        }
        return false;
    }
    
    private Utilizador existe(String user, List<Utilizador> lu)
    {
        for(Utilizador u : lu) {
            if( u != null && u.getUser().equals(user)) {
                return u;
            }
        }
        return null;
    }
    
}

