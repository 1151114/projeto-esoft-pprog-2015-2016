package gestaoexposicoes;

/**
 *
 * @author teofilomatos
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try
        {
            CentroExposicoes centro_exposicoes = new CentroExposicoes();

            MenuUI uiMenu = new MenuUI(centro_exposicoes);

            uiMenu.run();
        }
        catch( Exception e )
        {
            e.printStackTrace();
        }
    }
    
}
