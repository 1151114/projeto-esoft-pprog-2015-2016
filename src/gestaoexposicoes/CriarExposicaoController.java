package gestaoexposicoes;

import java.util.Date;
import java.util.List;

/**
 *
 * @author teofilomatos
 */
public class CriarExposicaoController {
    
    private final CentroExposicoes m_centro_exposicoes;
    private Exposicao m_exposicao;

    public CriarExposicaoController(CentroExposicoes centro_exposicoes)
    {
        m_centro_exposicoes = centro_exposicoes;
    }
    
     public void novaExposicao()
    {
        m_exposicao = m_centro_exposicoes.novaExposicao();
    }
 
    public List<Utilizador> getListaUtilizadores(){
        return m_centro_exposicoes.getUtilizadores();
    }
        
    public Exposicao setDados(String strTitulo, String strDescritivo, Date strDataIni, Date strDataFim, String strLocal, List<Utilizador> lstUtz )
    {
        m_exposicao.setTitulo(strTitulo);
        m_exposicao.setDescritivo(strDescritivo);
        m_exposicao.setPeriodo(strDataIni, strDataFim);
        m_exposicao.setLocal(strLocal);
        
        for(Utilizador u:lstUtz)
        {
            m_exposicao.addOrganizador(u);
        }

        if(m_centro_exposicoes.validaExposicao(m_exposicao))            
            return m_exposicao;
        else
            return null;
    }
    
    public boolean registaExposicao(){
        
        return m_centro_exposicoes.registaExposicao(m_exposicao);
        
    }
}
