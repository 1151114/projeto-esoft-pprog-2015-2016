

package gestaoexposicoes;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import utils.Utils;
/**
 *
 * @author Andre Pato
 */
public class DefinirFAEUI {
     private final CentroExposicoes m_centro_exposicoes;
     private final DefinirFAEController m_controllerFAE;
     
     public DefinirFAEUI(CentroExposicoes centro_exposicoes){
         m_centro_exposicoes = centro_exposicoes;
        m_controllerFAE = new DefinirFAEController();
     }
     
     public void run(){
         newFAE();
     }
     
     
}
