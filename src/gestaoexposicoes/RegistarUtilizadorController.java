
package gestaoexposicoes;

/**
 *
 * @author Andre Pato
 */
public class RegistarUtilizadorController {
    /**
     * cria um novo centro de exposicoes
     */
    private CentroExposicoes u_centroExposicao;
    /**
     * cria um novo utilizador
     */
    
    private Utilizador u_utilizador;
    /*+
    construtor completo
    */
    public RegistarUtilizadorController(CentroExposicoes u_centroExposicao){
        this.u_centroExposicao=u_centroExposicao;
    }
    /**
     * Cria um novo utilizador
     */
    public void novoUtilizador(){
        
    }
    /**
     * Define os dados do utilizador
     */
    public void setDados(){
        
    }
    /**
     * Mostra os dados do utilizador
     */
    public void getDados(){
        
    }
    /**
     * Valida os dados do utilizador
     */
    public void validaUtilizador(){
        
    }
    /**
     * mostra uma lista dos registos efetuados
     */
    public void getListaRegistos(){
        
    }
    /**
     * Define a decisao feita pelo utilizador do sistema
     */
    public void setDecisaoUtilizador(){
        
    }
}
