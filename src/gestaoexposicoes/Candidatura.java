/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestaoexposicoes;

/**
 *
 * @author Andre Pato
 */
public class Candidatura {
    /**
     * nome da empresa
     */
    private String nomeEmpresa;
    /**
     * morada da empresa
     */
    private String morada;
    /**
     * numero de telemovel da empresa
     */
    private int telemovel;
    /**
     * area de exposicao pretendida
     */
    private String area;
    /**
     * produtos a expor pela empresa
     */
    private String produto;
    /**
     * numero de convites pretendidos pela empresa
     */
    private int convites;
    /**
     * nome da empresa por omissao
     */
    private static final String NOME_EMPRESA_POR_OMISSAO="Sem nome da empresa";
    /**
     * morada da empresa por omissao
     */
    private static final String MORADA_POR_OMISSAO="Sem morada";
    /**
     * numero de telemovel da empresa por omissao
     */
    private static final int TELEMOVEL_POR_OMISSAO=0;
    /**
     * area de exposicao pretendida pela empresa por omissao
     */
    private static final String AREA_POR_OMISSAO="Sem area";
          /**
           * Produtos a expor pela empresa por omissao
    
    */  
    private static final String PRODUTO_POR_OMISSAO="Sem produtos";
    /**
     * quantidade de convites pretendidos pela empresa por omissao
     */
    private static final int CONVITES_POR_OMISSAO=0;
    /**
     * construtor vazio
     */
    public Candidatura(){
        nomeEmpresa=NOME_EMPRESA_POR_OMISSAO;
        morada=MORADA_POR_OMISSAO;
        telemovel=TELEMOVEL_POR_OMISSAO;
        area=AREA_POR_OMISSAO;
        produto=PRODUTO_POR_OMISSAO;
        convites=CONVITES_POR_OMISSAO;
    }
    /**
     * construtor completo
     * @param nomeEmpresa
     * @param morada
     * @param telemovel
     * @param areaExposicao
     * @param produtosExpor
     * @param numeroConvites 
     */
    public Candidatura(String nomeEmpresa,String morada,int telemovel,String areaExposicao,String produtosExpor,int numeroConvites ){
        this.nomeEmpresa=nomeEmpresa;
        this.morada=morada;
        this.telemovel=telemovel;
        this.area=areaExposicao;
        this.produto=produtosExpor;
        this.convites=numeroConvites;
    }

    
}

